package main;

public class Constants {
	public static final String THERMOSTAT = "TSTAT";
	public static final String BATTERY = "BATT";
	public static final String RED_LOW = "RED LOW";
	public static final String RED_HIGH = "RED HIGH";
	public static final int ALLOWABLE_VIOLATIONS = 2;
	public static final int FIVE_MINUTES = 300;
	public static final int NEWEST_TIMESTAMP = 2;
	public static final int OLDEST_TIMESTAMP = 0;
}
