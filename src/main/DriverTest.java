package main;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import org.junit.Test;

public class DriverTest {

	@Test
	public void testCreateRedAlert() {
		LocalDateTime ldt = LocalDateTime.now();
		HashMap<String, String> redAlert = Driver.createRedAlert("1", "LOW", "Comp", ldt);
		assertEquals(redAlert.get("satelliteId"), "1");
		assertEquals(redAlert.get("severity"), "LOW");
		assertEquals(redAlert.get("component"), "Comp");
		String formattedLDT = ldt.format(DateTimeFormatter.ofPattern("yyyyMMdd'T'HH:mm:ss.SSS'Z'"));
		assertEquals(redAlert.get("timestamp"), formattedLDT);
	}

}
