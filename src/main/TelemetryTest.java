package main;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

public class TelemetryTest {

	@Test
	public void testTelemetry() {
		String data = "20180101 23:01:09.521|1|17|15|9|8|7.8|BATT";
		Telemetry telemetry = new Telemetry(data);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
		LocalDateTime timestamp = LocalDateTime.parse("20180101 23:01:09.521", formatter);
		assertEquals(telemetry.getTimestamp(), timestamp);
		assertEquals(telemetry.getSatID(), "1");
		assertEquals(telemetry.getRedHigh(), 17.0, 0);
		assertEquals(telemetry.getYellowHigh(), 15.0, 0);
		assertEquals(telemetry.getYellowLow(), 9.0, 0);
		assertEquals(telemetry.getRedLow(), 8.0, 0);
		assertEquals(telemetry.getValue(), 7.8, 0);
		assertEquals(telemetry.getComponent(), "BATT");		
	}

}
