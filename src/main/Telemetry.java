package main;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Telemetry {
	private LocalDateTime timestamp;
	private String satID;
	private double redHigh;
	private double yellowHigh;
	private double yellowLow;
	private double redLow;
	private double value;
	private String component;
	
	public Telemetry(String data){
		//need to escape the regex character |
		String[] dataArray = data.split("\\|"); 
		setTimestamp(dataArray[0]);
		satID = dataArray[1];
		redHigh = Double.parseDouble(dataArray[2]);
		yellowHigh = Double.parseDouble(dataArray[3]);
		yellowLow = Double.parseDouble(dataArray[4]);
		redLow = Double.parseDouble(dataArray[5]);
		value = Double.parseDouble(dataArray[6]);
		component = dataArray[7].toUpperCase();
	}
	
	/**
	 * @return the timestamp
	 */
	public LocalDateTime getTimestamp(){
		return timestamp;
	}
	
	/**
	 * @param timestamp
	 */
	private void setTimestamp(String timestamp){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
		this.timestamp = LocalDateTime.parse(timestamp, formatter);
	}

	/**
	 * @return the satID
	 */
	public String getSatID() {
		return satID;
	}

	/**
	 * @return the redHigh
	 */
	public double getRedHigh() {
		return redHigh;
	}

	/**
	 * @return the yellowHigh
	 */
	public double getYellowHigh() {
		return yellowHigh;
	}

	/**
	 * @return the yellowLow
	 */
	public double getYellowLow() {
		return yellowLow;
	}

	/**
	 * @return the redLow
	 */
	public double getRedLow() {
		return redLow;
	}

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @return the component
	 */
	public String getComponent() {
		return component;
	}
}