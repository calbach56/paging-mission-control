package main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Carl
 * 
 */
public class Driver {
	private static HashMap<String, HashMap<String, ArrayList<LocalDateTime>>> alerts = new HashMap<String, HashMap<String, ArrayList<LocalDateTime>>>();
	
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		ArrayList<HashMap<String, String>> redAlerts = new ArrayList<HashMap<String, String>>();
		String filePath = ".\\PagingMissionControl.txt";
		File file = new File(filePath);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = "";
		// read the file line by line and determine if a red alert has occurred
		while((line = reader.readLine()) != null) {
			Telemetry telemetry = new Telemetry(line);
			String satID = telemetry.getSatID();
			// if the current satellite does not exist in the alerts[], build an empty alert
			if(alerts.get(satID) == null) {
				HashMap<String, ArrayList<LocalDateTime>> components = new HashMap<String, ArrayList<LocalDateTime>>();
				components.put(Constants.BATTERY, new ArrayList<LocalDateTime>());
				components.put(Constants.THERMOSTAT, new ArrayList<LocalDateTime>());
				alerts.put(satID, components);
			}
			
			// check if a single violation has occurred for either the battery or the thermostat 
			if(telemetry.getComponent().equals(Constants.BATTERY) && telemetry.getValue() < telemetry.getRedLow()) {
				alerts.get(satID).get(Constants.BATTERY).add(telemetry.getTimestamp());
				HashMap<String, String> tempRedAlert = checkAlerts();
				if(tempRedAlert != null) {
					redAlerts.add(tempRedAlert);
				}
			}
			else if(telemetry.getComponent().equals(Constants.THERMOSTAT) && telemetry.getValue() > telemetry.getRedHigh()) {
				alerts.get(satID).get(Constants.THERMOSTAT).add(telemetry.getTimestamp()); 
				HashMap<String, String> tempRedAlert = checkAlerts();
				if(tempRedAlert != null) {
					redAlerts.add(tempRedAlert);
				}
			}
		}
		
		reader.close();
		// gson is used to convert and format the redAlerts into pretty JSON format
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		System.out.println(gson.toJson(redAlerts));
	}
	
	/**
	 * @return single red alert object
	 */
	private static HashMap<String, String> checkAlerts() {
		HashMap<String, String> redAlert = null;
		// loop through the alerts and check for red alert occurrences
		for(Map.Entry<String, HashMap<String, ArrayList<LocalDateTime>>> entry : alerts.entrySet()) {
			String id = entry.getKey(); 
			ArrayList<LocalDateTime> battery = entry.getValue().get(Constants.BATTERY); 
			ArrayList<LocalDateTime> thermostat = entry.getValue().get(Constants.THERMOSTAT); 
			// red alert occurs when there are three violations within 5 minutes
			if(battery.size() > Constants.ALLOWABLE_VIOLATIONS) { 
				if(Duration.between(battery.get(Constants.OLDEST_TIMESTAMP), battery.get(Constants.NEWEST_TIMESTAMP)).getSeconds() <= Constants.FIVE_MINUTES) {
					redAlert = createRedAlert(id, Constants.RED_LOW, Constants.BATTERY, battery.get(Constants.OLDEST_TIMESTAMP));
				}

				// only keep the latest 2 violations
				battery.remove(Constants.OLDEST_TIMESTAMP);
			}
			// red alert occurs when there are three violations within 5 minutes
			else if(thermostat.size() > Constants.ALLOWABLE_VIOLATIONS) { 
				if(Duration.between(thermostat.get(Constants.OLDEST_TIMESTAMP), thermostat.get(Constants.NEWEST_TIMESTAMP)).getSeconds() <= Constants.FIVE_MINUTES) {
					redAlert = createRedAlert(id, Constants.RED_HIGH, Constants.THERMOSTAT, thermostat.get(Constants.OLDEST_TIMESTAMP));
				}
				
				// only keep the latest 2 violations
				thermostat.remove(Constants.OLDEST_TIMESTAMP);
			}
		}
		
		return redAlert;
	}
	
	/**
	 * @param satID
	 * @param severity
	 * @param component
	 * @param timestamp
	 * @return single red alert object
	 */
	protected static HashMap<String, String> createRedAlert(String satID, String severity, String component, LocalDateTime timestamp) {
		HashMap<String, String> redAlert = new HashMap<String, String>();
		redAlert.put("satelliteId", satID);
		redAlert.put("severity", severity);
		redAlert.put("component", component);
		String formattedTimestamp = timestamp.format(DateTimeFormatter.ofPattern("yyyyMMdd'T'HH:mm:ss.SSS'Z'"));
		redAlert.put("timestamp", formattedTimestamp);
		
		return redAlert;
	}
}


